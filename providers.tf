# Set the Azure provider source and version to get the latest one
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.51.0"
    }
  }
}

# Configure the Microsoft Azure provider
provider "azurerm" {
  features {}
}

# Configure the backend to be HTTP for the terraform state file
terraform {
  backend "http" {
  }
}
