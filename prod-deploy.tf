# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "prod-rg" {
  name     = var.resource_group_name
  location = var.resource_group_location

  tags = {
    environment = "production"
  }
}

# Create prod virtual network
resource "azurerm_virtual_network" "prod-vnet" {
  name                = var.virtual_network_name
  address_space       = ["10.0.0.0/24"]
  location            = azurerm_resource_group.prod-rg.location
  resource_group_name = azurerm_resource_group.prod-rg.name

  tags = {
    environment = "production"
  }
}

# Create prod subnet
resource "azurerm_subnet" "prod-subnet" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.prod-rg.name
  virtual_network_name = azurerm_virtual_network.prod-vnet.name
  address_prefixes     = ["10.0.0.8/29"]
}

# Create public IPs
resource "azurerm_public_ip" "prod-public_ip" {
  name                = var.public_ip_name
  location            = azurerm_resource_group.prod-rg.location
  resource_group_name = azurerm_resource_group.prod-rg.name
  allocation_method   = "Dynamic"

  tags = {
    environment = "production"
  }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "prod-nsg" {
  name                = var.network_security_group_name
  location            = azurerm_resource_group.prod-rg.location
  resource_group_name = azurerm_resource_group.prod-rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "production"
  }
}

# Create network interface
resource "azurerm_network_interface" "prod-nic" {
  name                = var.network_interface_name
  location            = azurerm_resource_group.prod-rg.location
  resource_group_name = azurerm_resource_group.prod-rg.name

  ip_configuration {
    name                          = "prod-nic-configuration"
    subnet_id                     = azurerm_subnet.prod-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.prod-public_ip.id
  }

  tags = {
    environment = "production"
  }
}

# Attach the security group to the network interface
resource "azurerm_network_interface_security_group_association" "prod-association" {
  network_interface_id      = azurerm_network_interface.prod-nic.id
  network_security_group_id = azurerm_network_security_group.prod-nsg.id
}

# Generate random text for a unique storage account name
resource "random_id" "prod-randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.prod-rg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "prod-storage" {
  name                     = "prod${random_id.prod-randomId.hex}"
  resource_group_name      = azurerm_resource_group.prod-rg.name
  location                 = azurerm_resource_group.prod-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "production"
  }
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "prod-linuxvm" {
  name                  = var.linux_virtual_machine_name
  location              = azurerm_resource_group.prod-rg.location
  resource_group_name   = azurerm_resource_group.prod-rg.name
  network_interface_ids = [azurerm_network_interface.prod-nic.id]
  size                  = "Standard_B1ls"

  os_disk {
    name                 = "myOsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }

  computer_name                   = "prod-vm"
  admin_username                  = var.vm_admin_name
  disable_password_authentication = true

  admin_ssh_key {
    username   = var.vm_admin_name
    public_key = var.default_ssh_key
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.prod-storage.primary_blob_endpoint
  }

  tags = {
    environment = "production"
  }
}